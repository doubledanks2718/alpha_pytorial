.. title: Introducing Learn2Code
.. slug: introducing-learn2code
.. date: 2016-06-02 15:26:29 UTC-04:00
.. tags:
.. category:
.. link:
.. description:
.. type: text

#########################
BiSA Presents: Learn2Code
#########################

*learn2code* is an organization within the Bioinformatics Student Association (BiSA) at
the University of Delaware. Our goal is two-fold:

   1) provide a platform for BiSA members to share their specializations among each other
   2) offer lessons which are tailored for working on scientific problems to the larger community of graduate students

Our primary objective is to provide tutorials tailored to fit students working on
scientific problems. There are many tutorials available for any computer language;
however, these tutorials emphasize learning the language for its own sake. In my
work as a graduate student I write code to solve problems as they come up. My experience
with learning new languages or something new about that language has been:

   1) figure out if I need to learn a new language/technique
   2) learn the language/technique through a generic tutorial
   3) figure out how to use what I just spent my time learning

I dread learning anything new because the hardest part: implementing what I just learned
is the most time consuming and seems to be conveniently left out by nearly every tutorial.
