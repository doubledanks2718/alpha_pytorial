.. Pytorials documentation master file, created by
   sphinx-quickstart on Mon May 30 18:53:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pytorials
====================

Contents:

.. toctree::
   :maxdepth: 1

   the_path_variable.rst
   getting_started.rst
   jupyter_notebook_and_basics.rst
   parse_gff3_file.rst
   documentation_and_improved_parser.rst
   using_numpy.rst
   using_pandas.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

