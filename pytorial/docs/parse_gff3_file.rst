.. _parsing_gff3:

===================
Parsing a GFF3 File
===================

I feel that a gff3 file is relevant to most of the audience rather than some
random data set such as weather data or financial data. The gff3 file format is
fairly simple so it provides a good place to use some of our basic skills.

Fresh Workspace
===============

Before we start writing our file parser let's open a fresh workspace for ourselves.
Make a new ``Jupyter Notebook`` by going to the **Home** tab in our web browser.
In our **Home** Page do New --> Python 3. This will open up an ``Untitled`` notebook.
Select our new notebook and do File --> Rename. Let's call it ``file_parsing``.
We should have something which looks like:

.. image:: images/fresh_notebook.PNG

Inspect the File
~~~~~~~~~~~~~~~~

Whenever we are writing a file parser it is a good idea to open the file up
and look at it so we have an idea of what to expect. We can view it within
our ``Jupyter Notebook``. Go to the **Home** tab and click on ``example.gff3``.
It looks like a fairly simple tab delimited file so let's start writing our parser.

.. _open_file_python:

Opening a File in Python
========================

We will use the :py:func:`open` function which requires two parameters

   1) the name of the file as a string
   2) the mode as a string

By *mode* we mean whether we want to read the file, write to the file or some
kind of combination. The read mode is 'r'.

.. code-block:: python

   >>> gff_file = open('example.gff3', 'r')
   >>> gff_file
   <_io.TextIOWrapper name='example.gff3' mode='r' encoding='cp1252'>

Notice that we cannot simply evaluate ``gff_file`` to see the contents of the file.
We must do more to access the data in the ``gff_file`` object. Our strategy is to
extract the text data in the ``gff_file`` object and put it into a ``list``.

.. code-block:: python

   >>> gff_data = []
   >>> for line in gff_file:
   ...   gff_data.append(line)
   >>> gff_file.close()

Let's see what ``gff_data`` is.

.. code-block:: python

   >>> gff_data
   ['edit_test.fa\t.\tgene\t500\t2610\t.\t+\t.\tID=newGene\n',
   'edit_test.fa\t.\tmRNA\t500\t2385\t.\t+\t.\tParent=newGene;Namo=reinhard+did+this;Name=t1%28newGene%29;ID=t1;uri=http%3A//www.yahoo.com\n',
   'edit_test.fa\t.\tfive_prime_UTR\t500\t802\t.\t+\t.\tParent=t1\n',
   'edit_test.fa\t.\tCDS\t803\t1012\t.\t+\t.\tParent=t1\n',
   'edit_test.fa\t.\tthree_prime_UTR\t1013\t1168\t.\t+\t.\tParent=t1\n',
   'edit_test.fa\t.\tthree_prime_UTR\t1475\t1654\t.\t+\t.\tParent=t1\n',
   'edit_test.fa\t.\tthree_prime_UTR\t1720\t1908\t.\t+\t.\tParent=t1\n',
   'edit_test.fa\t.\tthree_prime_UTR\t2047\t2385\t.\t+\t.\tParent=t1\n',
   'edit_test.fa\t.\tmRNA\t1050\t2610\t.\t+\t.\tParent=newGene;Name=t2%28newGene%29;ID=t2\n',
   'edit_test.fa\t.\tCDS\t1050\t1196\t.\t+\t.\tParent=t2\n',
   'edit_test.fa\t.\tCDS\t1472\t1651\t.\t+\t.\tParent=t2\n',
   'edit_test.fa\t.\tCDS\t1732\t2610\t.\t+\t.\tParent=t2\n',
   'edit_test.fa\t.\tmRNA\t1050\t2610\t.\t+\t.\tParent=newGene;Name=t3%28newGene%29;ID=t3\n',
   'edit_test.fa\t.\tCDS\t1050\t1196\t.\t+\t.\tParent=t3\n',
   'edit_test.fa\t.\tCDS\t1472\t1651\t.\t+\t.\tParent=t3\n',
   'edit_test.fa\t.\tCDS\t1732\t2610\t.\t+\t.\tParent=t3']

Successful. It looks like we have the text from our file. Now we will do some
extra steps to put the raw text into a useful form. Let's use the first line of
``gff_data`` to work with. We will generalize to the other lines later. First
let's remove the end of line character: ``\n`` by using the :py:func:`strip` function.

.. code-block:: python

   >>> gff_data[0].strip()
   'edit_test.fa\t.\tgene\t500\t2610\t.\t+\t.\tID=newGene'

Alright good, the ``\n`` is gone. Now we will use the :py:func:`split` function
to break up the string into separate entries.

.. code-block:: python

   >>> gff_data[0].split('\t')
   ['edit_test.fa', '.', 'gene', '500', '2610', '.', '+', '.', 'ID=newGene\n']

Well the :py:func:`split` did its job but the end of line character is back.
Let's trying applying both functions at the same time and see what we get.

.. code-block:: python

   >>> gff_data[0].strip().split('\t')
   ['edit_test.fa', '.', 'gene', '500', '2610', '.', '+', '.', 'ID=newGene']

Now we have what we want. Let's write a loop to generalize this to our entire
data set. We will write our loop with a ``break`` keyword and look at the first
element. Once we have confirmed that our loop is working correctly we will remove
the ``break`` keyword and iterate over all of the data.

.. code-block:: python

   >>> parsed_gff_data = []
   >>> for line in gff_data:
   ...   parsed_line = line.strip().split('\t')
   ...   parsed_gff_data.append(parsed_line)
   ...   break
   >>> parsed_gff_data
   [['edit_test.fa', '.', 'gene', '500', '2610', '.', '+', '.', 'ID=newGene']]

Now that we have confirmed that our loop works for a single element let's remove
the ``break`` keyword and let the iteration run over all the elements.

.. code-block:: python

   >>> parsed_gff_data = []
   >>> for line in gff_data:
   ...   parsed_line = line.strip().split('\t')
   ...   parsed_gff_data.append(parsed_line)
   >>> parsed_gff_data
   [['edit_test.fa', '.', 'gene', '500', '2610', '.', '+', '.', 'ID=newGene'],
    ['edit_test.fa',
     '.',
     'mRNA',
     '500',
     '2385',
     '.',
     '+',
     '.',
     'Parent=newGene;Namo=reinhard+did+this;Name=t1%28newGene%29;ID=t1;uri=http%3A//www.yahoo.com'],
    ['edit_test.fa',
     '.',
     'five_prime_UTR',
     '500',
     '802',
     '.',
     '+',
     '.',
     'Parent=t1'],
    ['edit_test.fa', '.', 'CDS', '803', '1012', '.', '+', '.', 'Parent=t1'],
    ['edit_test.fa',
     '.',
     'three_prime_UTR',
     '1013',
     '1168',
     '.',
     '+',
     '.',
     'Parent=t1'],
    ['edit_test.fa',
     '.',
     'three_prime_UTR',
     '1475',
     '1654',
     '.',
     '+',
     '.',
     'Parent=t1'],
    ['edit_test.fa',
     '.',
     'three_prime_UTR',
     '1720',
     '1908',
     '.',
     '+',
     '.',
     'Parent=t1'],
    ['edit_test.fa',
     '.',
     'three_prime_UTR',
     '2047',
     '2385',
     '.',
     '+',
     '.',
     'Parent=t1'],
    ['edit_test.fa',
     '.',
     'mRNA',
     '1050',
     '2610',
     '.',
     '+',
     '.',
     'Parent=newGene;Name=t2%28newGene%29;ID=t2'],
    ['edit_test.fa', '.', 'CDS', '1050', '1196', '.', '+', '.', 'Parent=t2'],
    ['edit_test.fa', '.', 'CDS', '1472', '1651', '.', '+', '.', 'Parent=t2'],
    ['edit_test.fa', '.', 'CDS', '1732', '2610', '.', '+', '.', 'Parent=t2'],
    ['edit_test.fa',
     '.',
     'mRNA',
     '1050',
     '2610',
     '.',
     '+',
     '.',
     'Parent=newGene;Name=t3%28newGene%29;ID=t3'],
    ['edit_test.fa', '.', 'CDS', '1050', '1196', '.', '+', '.', 'Parent=t3'],
    ['edit_test.fa', '.', 'CDS', '1472', '1651', '.', '+', '.', 'Parent=t3'],
    ['edit_test.fa', '.', 'CDS', '1732', '2610', '.', '+', '.', 'Parent=t3']]

It looks like we have succeeded. It is likely that we will encounter gff3 file
in the future so let's make our code into a function which so we can re-use it
without having to re-write the entire loop. We create a function with the ``def``
keyword.

.. _parser_function:

.. code-block:: python

   >>> def gff3_parser(gff_file_name):
   ...   gff_file = open(gff_file_name)
   ...   parsed_gff_data = []
   ...   for line in gff_file:
   ...      parsed_line = line.strip().split('\t')
   ...      parsed_gff_data.append(parsed_line)
   ...   gff_file.close()
   ...   return parsed_gff_data

Now let's test our function out to see if it gives us the same results. We could always
compare the value we get from the function with the one we got from our loop.
However, for a very large file this is not practical. Instead we will use the
comparison operator ``==``.

.. code-block:: python

   >>> gff_data_from_function = gff3_parser('example.gff3')
   >>> gff_data_from_function == parsed_gff_data
   True

This is encouraging. Our function behaves exactly as we have planned. Next we
will work on a more complicated gff3 file. We will use the more complicated file
to improve our function so it can be used in a larger number of cases. However,
before we do let's make a ``git`` repository for version control. If we make
a mistake we can simply rollback to a previous version which we know worked
begin from the working version.

.. _first_repo:

Our First Repository
====================

Open a terminal and navigate to the same directory we are working in. Then type
``git init`` and press ``Enter``. You should see something similar to the code below.

.. code-block:: console

   $ cd /c/users/doubledanks/pycharmprojects/l2c_tutorials
   $ git init
   Initialized empty Git repository in C:/Users/DoubleDanks/pycharmprojects/l2c_tutorials/.git/

At the bare minimum ``git`` needs your name and email. Set these by doing:

.. code-block:: console

   $ git config user.name "John J. Dougherty III"
   $ git config user.email jjdoc@udel.edu

Now we have to add the files we want to track to our repository using the ``git add``
command.

.. code-block:: console

   $ git add file_parsing.ipynb
   $ git add example.gff3

We might have some files in the directory which we do not want to include in our
repository. For example the .ipynb_checkpoints folder is not necessary. ``git`` uses
a special file ``.gitignore`` to ignore certain files. Let's use the ``nano`` editor
to make a ``.gitignore`` file.

.. code-block:: console

   $ nano .gitignore

   .gitignore
   .ipynb_checkpoints/

Then press ``Ctrl+X`` followed by ``y`` to write the file. Now type ``git status``
and press ``Enter``. You will see something slightly different but it should be
similar to the example shown below.

.. code-block:: console

   $ git status
   Changes to be committed:
     (use "git rm --cached <file>..." to unstage)

           new file:   pytorial/code_examples/example.gff3
           new file:   pytorial/code_examples/file_parsing.ipynb
           new file:   pytorial/code_examples/file_parsing_advanced.ipynb
           new file:   pytorial/code_examples/second_example.gff3
           new file:   pytorial/docs/fresh_notebook.PNG
           new file:   pytorial/docs/getting_started.rst
           new file:   pytorial/docs/index.rst
           new file:   pytorial/docs/jupyter_notebook_and_basics.rst
           new file:   pytorial/docs/jupyter_notebook_home.PNG
           new file:   pytorial/docs/parse_gff3_file.rst
           new file:   shell_and_nano/env_variable_menu.PNG
           new file:   shell_and_nano/nano_in_terminal.PNG
           new file:   shell_and_nano/path_exercise.PNG
           new file:   shell_and_nano/path_variable_value.PNG
           new file:   shell_and_nano/sys_env_variables.PNG
           new file:   shell_and_nano/the_path_variable.rst

   Changes not staged for commit:
     (use "git add <file>..." to update what will be committed)
     (use "git checkout -- <file>..." to discard changes in working directory)

           modified:   pytorial/docs/parse_gff3_file.rst

Now we will commit these changes to our repository using the ``git commit`` command.

.. code-block:: console

   $ git commit -m "Our first commit!"
   [master (root-commit) e506cf1] Initial commit.
   16 files changed, 1443 insertions(+)
   create mode 100644 pytorial/code_examples/example.gff3
   create mode 100644 pytorial/code_examples/file_parsing.ipynb
   create mode 100644 pytorial/code_examples/file_parsing_advanced.ipynb
   create mode 100644 pytorial/code_examples/second_example.gff3
   create mode 100644 pytorial/docs/fresh_notebook.PNG

Again, you will not see exactly the same thing; however, you should see something
similar. What happens if we run the ``git status`` command again?

.. code-block:: console

   $ git status
   On branch master
   nothing to commit, working directory clean

.. note::

   ``git`` is generally used for tracking versions of code. If you need to track
   changes to a large data set there are much better tools to use than ``git``.

.. _end_parse_gff3_file.rst:

Next Lesson
===========

In the next part of this lesson we will learn how to include documentation in
our ``gff3_parser`` function. Documentation is extremely important and should not
be taken lightly. Please learn from my mistakes. I will show you how to write
good documentation with minimal effort. Then we will improve our function to
handle a more complicated GFF3 file.




.. _read_shelves:

Retrieving Data Stored with ``shelve``
======================================

When we use :py:mod:`shelve` to store data it creates three files. For example we
called our file ``parsed_gff3_data``. :py:mod:`shelve` creates three files:

   + ``parsed_gff3_data.bak``
   + ``parsed_gff3_data.dir``
   + ``parsed_gff3_data.dat``

When we want to read the file just use the root and ignore the .bak, .dir or .dat
extensions. For example:

.. code-block:: py

   >>> import shelve
   >>> stored_data = shelve.open('parsed_gff3_data.')

You access the data inside of a :py:class:`shelve` using a ``key``. To see the keys
of ``stored_data`` we can use the :py:func:`list` function.

.. code-block:: py

   >>> list(stored_data)
   ['sequence', 'gff_data', 'header']
