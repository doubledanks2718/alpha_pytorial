.. _writing_documentation:

=====================
Documenting Your Code
=====================

Documentation is something that I never considered a priority. I saw it as extra
work and I was already pressed for time so I simply did not write it. I sincerely
regret that decision. Python functions have a special attribute called a
``docstring`` which gives a brief description of what the function does. They are
incredibly helpful and I *strongly* encourage you to write them. I will show
you how to write *effective* docstrings with minimal effort.

.. warning::

   Do **not** mix comments in with your code. You have probably been told over
   and over that you must comment your code. This is a mistake. You will change
   your code and you **will** forget to change your comments. You will wind up
   confusing yourself and others who view your code. Write the information in
   the docstring. Do not use comments.

Fresh Notebook
==============

Let's get started with a fresh ``Jupyter Notebook``. Let's call it
``documentation_and_improved_parser``. Copy and paste our ``gff3_parser`` from
the previous notebook. Now let's write the docstring to the ``gff3_parser`` function.
A good docstring is a very short description of what the function does with descriptions
of each of the function's parameters. In this case we only have a single parameter
``gff_file_name``. The final component of a docstring is an example of how
the function is used. We can just copy and paste our work from the previous notebook
for an example.

.. _gff3_parser_docstring:

.. code-block:: python

   def gff3_parser(gff_file_name):
       """
       Reads a GFF3 file and returns the data separated by columns.
       Assumes GFF3 file is tab-delimited.

       :parameter str gff_file_name: gff3 file name as a string

       example
       ~~~~~~~

       Given a file that looks like this
       edit_test.fa	.	gene	500	2610	.	+	.	ID=newGene
       edit_test.fa	.	mRNA	500	2385	.	+	.	Parent=newGene;Namo=reinhard+did+this;Name=t1%28newGene%29;ID=t1;uri=http%3A//www.yahoo.com

       gff3_parser parses the raw text into separate columns

       >>> gff3_parser('example.gff3')
       [['edit_test.fa', '.', 'gene', '500', '2610', '.', '+', '.', 'ID=newGene'],
        ['edit_test.fa',
         '.',
         'mRNA',
         '500',
         '2385',
         '.',
         '+',
         '.',
         'Parent=newGene;Namo=reinhard+did+this;Name=t1%28newGene%29;ID=t1;uri=http%3A//www.yahoo.com'],
         ]
       """
       gff_file = open(gff_file_name, 'r')
       parsed_gff_data = []
       for line in gff_file:
           parsed_line = line.strip().split('\t')
           parsed_gff_data.append(parsed_line)
       return parsed_gff_data

That's it! This is a perfectly valid docstring. It tells us what the function does,
explains what the parameter is and shows an example of how the function is used.
I promise you that taking an extra minute or two to write this docstrings
as you define your functions will save you hours of torment in the future. Documenting
our function is worthy of a ``git commit``.

.. code-block:: console

   $ git add documentation_and_improved_parser.ipynb
   $ git commit -am "Added docstring to gff3_parser function."
   [master d61c854] Added docstring to gff3_parser function.
   10 files changed, 114374 insertions(+), 41 deletions(-)

.. _general_gff3_file_parser:

Parsing a More Complicated GFF3 File
====================================

The first file we parsed was an incredibly simple gff3 file. We will now work
on a much more complicated file. I have shortened the file ``second_example.gff3``.
The actual file is many, many times larger; however, this truncated form has all the
essential features of the larger file. Open it up in your text editor and take a look at it.
We can divide the file into three sections: 1) header, 2) gff_data and 3) sequence.
We will work on parsing each of these parts one at a time. Then we will combine the
results at the end.

Let's start with the header portion. We will start as usual by opening the file
and reading its contents into a list. However, this time we will use the ``with`` statement.
``with`` is a context manager. It will automatically close the file for us so we
do not have to worry about forgetting to close a file. Notice that the header
rows are preceded by the string "##". We can take advantage of that.

.. code-block:: python

   >>> header = []
   >>> with open('second_example.gff3', 'r') as gff_file:
   ...   for line in gff_file:
   ...      if line[:2] == '##':
   ...         header.append(line.strip())
   >>> header
   ['##gff-version 3',
   '##sequence-region 2L 1 23011544',
   '##species  http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7227',
   '##feature-ontology ftp://ftp.flybase.org/releases/FB2013_06/precomputed_files/ontologies/so.obo.zip',
   '##genome-build FlyBase r5.54',
   '###',
   '##FASTA']

This is almost what we wanted but not quite. We have taken some of the sequence portion.
Let's use another condition in our if statement.

.. code-block:: python

   >>> header = []
   >>> with open('second_example.gff3', 'r') as gff_file:
   ...   for line in gff_file:
   ...      if line[:2] == '##' and line[2] != '#':
   ...         header.append(line.strip())
   >>> header
   ['##gff-version 3',
   '##sequence-region 2L 1 23011544',
   '##species  http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7227',
   '##feature-ontology ftp://ftp.flybase.org/releases/FB2013_06/precomputed_files/ontologies/so.obo.zip',
   '##genome-build FlyBase r5.54',
   '##FASTA']

Almost! We only need to add one more condition.

.. code-block:: python

   >>> header = []
   >>> with open('second_example.gff3', 'r') as gff_file:
   ...   for line in gff_file:
   ...      if line[:2] == '##' and line[2] != '#' and line[2] != 'F':
   ...         header.append(line.strip())
   >>> header
   ['##gff-version 3',
   '##sequence-region 2L 1 23011544',
   '##species  http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7227',
   '##feature-ontology ftp://ftp.flybase.org/releases/FB2013_06/precomputed_files/ontologies/so.obo.zip',
   '##genome-build FlyBase r5.54']

Perfect. We have the header and only the header. Now let's work on the gff_data.
Observe that each column of the gff_data begins with the string '2L'. The sequence
portion begins with '>2L' so we can simply ignore all the lines which do not begin
with '2L'.

.. code-block:: python

   >>> gff_data = []
   >>>   with open('second_example.gff3', 'r') as gff_file:
   ...      for line in gff_file:
   ...         if line[:2] != '2L':
   ...            pass
   ...         else:
   ...            gff_data.append(line.strip().split('\t'))
   >>> gff_data[0]
   ['2L',
   'FlyBase',
   'chromosome_arm',
   '1',
   '23011544',
   '.',
   '.',
   '.',
   'ID=2L;Name=2L;Dbxref=REFSEQ:NT_033779,GB:AE014134']

The gff_data section was very simple. However, the sequence section could be tricky.
However, observe that the sequence information is in a DNA alphabet. So all we need
to do is to take all the lines beginning with '>2L', 'A', 'C', 'G', 'T'. This will
look very clumsy; however, it will work.

.. code-block:: python

   >>> sequence = []
   >>> with open('second_example.gff3', 'r') as gff_file:
   ...   for line in gff_file:
   ...     if line[0] == '>' or line[0] == 'A' or line[0] == 'C' or line[0] == 'G' or line[0] == 'T':
   ...         sequence.append(line.strip())
   ... sequence
   ['>2L',
    'CGACAATGCACGACAGAGGAAGCAGAACAGATATTTAGATTGCCTCTCATTTTCTCTCCCATATTATAGGGAGAAATATG',
    'ATCGCGTATGCGAGAGTAGTGCCAACATATTGTGCTCTTTGATTTTTTGGCAACCCAAAATGGTGGCGGATGAACGAGAT',
    'GATAATATATTCAAGTTGCCGCTAATCAGAAATAAATTCATTGCAACGTTAAATACAGCACAATATATGATCGCGTATGC',
    'GAGAGTAGTGCCAACATATTGTGCTAATGAGTGCCTCTCGTTCTCTGTCTTATATTACCGCAAACCCAAAAAGACAATAC',
    'ACGACAGAGAGAGAGAGCAGCGGAGATATTTAGATTGCCTATTAAATATGATCGCGTATGCGAGAGTAGTGCCAACATAT',
    'TGTGCTCTCTATATAATGACTGCCTCTCATTCTGTCTTATTTTACCGCAAACCCAAATCGACAATGCACGACAGAGGAAG',
    'CAGAACAGATATTTAGATTGCCTCTCATTTTCTCTCCCATATTATAGGGAGAAATATGATCGCGTATGCGAGAGTAGTGC',
    'CAACATATTGTGCTCTTTGATTTTTTGGCAACCCAAAATGGTGGCGGATGAACGAGATGATAATATATTCAAGTTGCCGC',
    'TAATCAGAAATAAATTCATTGCAACGTTAAATACAGCACAATATATGATCGCGTATGCGAGAGTAGTGCCAACATATTGT',
    'GCTAATGAGTGCCTCTCGTTCTCTGTCTTATATTACCGCAAACCCAAAAAGACAATACACGACAGAGAGAGAGAGCAGCG',
    'GAGATATTTAGATTGCCTATTAAATATGATCGCGTATGCGAGAGTAGTGCCAACATATTGTGCTCTCTATATAATGACTG',
    'CCTCTCATTCTGTCTTATTTTACCGCAAACCCAAATCGACAATGCACGACAGAGGAAGCAGAACAGATATTTAGATTGCC',
    'TCTCATTTTCTCTCCCATATTATAGGGAGAAATATGATCGCGTATGCGAGAGTAGTGCCAACATATTGTGCTCTTTGATT',
    'TTTTGGCAACCCAAAATGGTGGCGGATGAACGAGATGATAATATATTCAAGTTGCCGCTAATCAGAAATAAATTCATTGC',
    'AACGTTAAATACAGCACAATATATGATCGCGTATGCGAGAGTAGTGCCAACATATTGTGCTAATGAGTGCCTCTCGTTCT',
    'CTGTCTTATATTACCGCAAACCCAAAAAGACAATACACGACAGAGAGAGAGAGCAGCGGAGATATTTAGATTGCCTATTA',
    'AATATGATCGCGTATGCGAGAGTAGTGCCAACATATTGTGCTCTCTATATAATGACTGCCTCTCATTCTGTCTTATTTTA',
    'CCGCAAACCCAAATCGACAATGCACGACAGAGGAAGCAGAACAGATATTTAGATTGCCTCTCATTTTCTCTCCCATATTA',
    'TAGGGAGAAATATGATCGCGTATGCGAGAGTAGTGCCAACATATTGTGCTCTTTGATTTTTTGGCAACCCAAAATGGTGG',
    'CGGATGAACGAGATGATAATATATTCAAGTTGCCGCTAATCAGAAATAAATTCATTGCAACGTTAAATACAGCACAATAT',
    'ATGATCGCGTATGCGAGAGTAGTGCCAACATATTGTGCTAATGAGTGCCTCTCGTTCTCTGTCTTATATTACCGCAAACC',
    'CAAAAAGACAATACACGACAGAGAGAGAGAGCAGCGGAGATATTTAGATTGCCTATTAAATATGATCGCGTATGCGAGAG',
    'TAGTGCCAACATATTGTGCTCTCTATATAATGACTGCCTCTCATTCTGTCTTATTTTACCGCAAACCCAAATCGACAATG',
    'CACGACAGAGGAAGCAGAACAGATATTTAGATTGCCTCTCATTTTCTCTCCCATATTATAGGGAGAAATATGATCGCGTA',
    'TGCGAGAGTAGTGCCAACATATTGTGCTCTTTGATTTTTTGGCAACCCAAAATGGTGGCGGATGAACGAGATGATAATAT',
    'ATTCAAGTTGCCGCTAATCAGAAATAAATTCATTGCAACGTTAAATACAGCACAATATATGATCGCGTATGCGAGAGTAG',
    'TGCCAACATATTGTGCTAATGAGTGCCTCTCGTTCTCTGTCTTATATTACCGCAAACCCAAAAAGACAATACACGACAGA',
    'GAGAGAGAGCAGCGGAGATATTTAGATTGCCTATTAAATATGATCGCGTATGCGAGAGTAGTGCCAACATATTGTGCTCT',
    'CTATATAATGACTGCCTCTCATTCTGTCTTATTTTACCGCAAACCCAAATCGACAATGCACGACAGAGGAAGCAGAACAG']

Albeit a bit clumsy our approach was straightforward. Now we have the file parsed into
three separate pieces of data:

1. header
2. gff_data
3. sequence

We have performed quite a bit of work to get to this point. It would be very nice if we could easily
save our progress. Indeed we can! The ``shelve`` library allows you to store
arbitrary ``Python`` objects.

.. _save_with_shelve:

The ``shelve`` Module
=====================

.. code-block:: python

   >>> import shelve
   >>> gff_data_shelve = shelve.open('parsed_gff3_data')
   >>> gff_data_shelve['header'] = header
   >>> gff_data_shelve['gff_data'] = gff_data
   >>> gff_data_shelve['sequence'] = sequence
   >>> gff_data_shelve.close()

Notice that we have three files in our directory. parsed_gff3_data.bak, .dir and .dat.
Let's add these to our repository in the case that we decide to take a break
or we need to stop for whatever reason.

.. code-block:: console

   $ git add parsed_gff3_data*
   $ git status
   On branch master
   Changes to be committed:
     (use "git reset HEAD <file>..." to unstage)

           new file:   code_examples/parsed_gff3_data.bak
           new file:   code_examples/parsed_gff3_data.dat
           new file:   code_examples/parsed_gff3_data.dir
   $ git commit -am "Parsed second_example.gff3 into three separate objects. Saved
   results using the shelve module in parsed_gff3_data."
   [master e6125f2] Parsed second_example.gff3 into three separate objects. Saved results to parsed_gff3_data shelve.
    3 files changed, 6 insertions(+)
    create mode 100644 pytorial/code_examples/parsed_gff3_data.bak
    create mode 100644 pytorial/code_examples/parsed_gff3_data.dat
    create mode 100644 pytorial/code_examples/parsed_gff3_data.dir
    2 files changed, 339 insertions(+), 29 deletions(-)
   $ git status
   On branch master
   nothing to commit, working directory clean

Refining our if Statements
==========================

We will stop here. I feel that we have learned a great deal of how to read files
and extract useful information from their raw contents. As an exercise I would
like you to perform all of the file parsing within a single loop instead of three
separate loops. Then re-define your function ``gff3_parser`` with the single loop
and perform a git commit. I will post the solution on the `learn2code Ryver`_.
Please feel free to message me for a hint or talk amongst each other.

.. _learn2code Ryver:




