
.. _using_numpy:

===========
Using Numpy
===========

:mod:`numpy` short for *Numerical Python* is a fundamental package for many
scientific computing applications. :mod:`numpy` provides access to the :class:`array`
If we wanted a multi-dimensional array in pure ``Python`` we would have to use
a list of lists or something similar. :mod:`numpy` arrays are much easier to use
and are generally better in terms of computational speed (performance). We will
learn about a package called :mod:`pandas` which builds on top of the :mod:`numpy` array for even better data structures.

.. note::

   As of Python 3.5 the NumPy array is actually a built-in type.

.. _numpy_start_jupyter:

Fire Up the Jupyter Notebook
============================

Download a local copy of the files :file:`numpy_example_array.txt` and
:file:`numpy_mixed_types.txt`. I made a folder called ``working`` and put the
examples in there. Navigate to your ``working`` folder and start the ``Jupyter Notebook``

.. code-block:: console
   :caption: Start up the ``Jupyter Notebook``

   $ cd /c/users/doubledanks/working
   $ ls
   numpy_example_array.txt  numpy_mixed_types.txt
   $ jupyter notebook
   [I 20:16:59.011 NotebookApp] Serving notebooks from local directory: C:\Users\DoubleDanks\working\
   [I 20:16:59.011 NotebookApp] 0 active kernels
   [I 20:16:59.011 NotebookApp] The Jupyter Notebook is running at: http://localhost:8888/
   [I 20:16:59.011 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).

.. _navigating_numpy_arrays:

Navigating Numpy Arrays
=======================

Numpy arrays support more sophisticated iteration patterns than Python lists.
A Numpy array can be :math:`n`\-dimensional but in most cases we are usually
working with two or three dimensional arrays. We will introduce the basics of
accessing the elements of Numpy arrays.

.. _read_text_file:

Reading a Text File Using Numpy
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The first example we will look at is a space-delimited text file i.e. a single whitespace separates the columns.
We will use the :py:func:`loadtxt`:py func which allows us to specify the
delimiter using the ``delim`` keyword.

.. code-block:: py
   :caption: Read a text file using numpy

   >>> import numpy as np
   >>> example_array = np.loadtxt('numpy_example_array.txt', delimiter=' ')
   array([[ 0.,  0.,  0., ...,  0.,  0.,  0.],
          [ 0.,  0.,  0., ...,  0.,  0.,  0.],
          [ 0.,  0.,  0., ...,  1.,  1.,  1.],
          ...,
          [ 0.,  0.,  1., ...,  1.,  0.,  1.],
          [ 1.,  0.,  0., ...,  0.,  1.,  0.],
          [ 0.,  1.,  0., ...,  1.,  1.,  1.]])

To determine the dimensionality of the array we just loaded we can check the
``shape`` attribute. The ``shape`` attribute will return a tuple of (row, column)

.. code-block:: py
   :caption: Check shape of an array

   >>> example_array.shape
   (20, 943)

So ``example_array`` has twenty rows and 943 columns. Also we can check the
data type of an array with the ``dtype`` attribute.

.. code-block:: py
   :caption: Data type

   >>> example_array.dtype
   dtype('float64')

You will recognize that :type:`float64` is not a Python data type.
In fact it is a :mod:`numpy` data type.

.. _indexing:

Indexing
^^^^^^^^

In this section we will learn about how to access the elements of arrays.

.. code-block:: py
   :caption: Single elements

   >>> example_array[0, 1] # first row second column
   0.0
   >>> example_array[2, 2] # third row third column
   0.0
   >>> example_array[19, 942] # last row last column
   1.0
   >>> example_array[-1, -1] # last row last column
   1.0

.. _slicing:

Slicing
^^^^^^^

We can take 'slices` or subsets of the array.

.. code-block:: py
   :caption: Slices

   >>> example_array[2:10, :]
   array([[ 0.,  0.,  0., ...,  1.,  1.,  1.],
          [ 1.,  0.,  0., ...,  1.,  0.,  0.],
          [ 0.,  0.,  0., ...,  0.,  0.,  0.],
          ...,
          [ 0.,  0.,  0., ...,  0.,  0.,  0.],
          [ 0.,  1.,  0., ...,  1.,  0.,  1.],
          [ 0.,  0.,  0., ...,  1.,  1.,  1.]])
   >>> example_array[2:10, :].shape
   (8, 943)

These are rows 2, 3, 4, 5, 6, 7, 8, 9.

.. code-block:: py
   :caption: More slicing

   >>> example_array[:, 10:50:10]
   array([[ 0.,  0.,  1.,  0.],
       [ 0.,  0.,  0.,  0.],
       [ 0.,  0.,  0.,  0.],
       [ 0.,  0.,  0.,  0.],
       [ 0.,  1.,  0.,  0.],
       [ 0.,  1.,  1.,  1.],
       [ 1.,  1.,  0.,  0.],
       [ 0.,  1.,  1.,  1.],
       [ 1.,  2.,  0.,  0.],
       [ 1.,  1.,  0.,  1.],
       [ 0.,  0.,  0.,  0.],
       [ 0.,  0.,  0.,  0.],
       [ 0.,  1.,  0.,  0.],
       [ 0.,  0.,  0.,  1.],
       [ 0.,  0.,  0.,  1.],
       [ 0.,  1.,  0.,  1.],
       [ 0.,  1.,  0.,  1.],
       [ 1.,  0.,  0.,  0.],
       [ 1.,  0.,  1.,  0.],
       [ 0.,  2.,  0.,  0.]])
   >>> example_array[:, 10:50:5].shape
   (20, 4)

This is every tenth column: 10, 20, 30, 40.

.. _operating_on_numpy_arrays:

Operating on Numpy Arrays
=========================

Operating on Numpy arrays is in general faster than Python lists. There are
convenient functions which operate on entire rows or columns at a time. Axis 0
will iterate over the columns of this array and axis 1 will iterate over the rows.

.. code-block:: py
   :caption: Row or column operations

   >>> column_means = np.mean(example_array, axis=0)
   >>> column_means
   array([ 0.25,  0.1 ,  0.15,  0.05,  0.25,  0.7 ,  0.2 ,  0.85,  0.7 ,
        0.3 ,  0.25,  0.75,  0.2 ,  0.55,  0.2 ,  0.95,  0.05,  0.55,
        0.1 ,  0.1 ,  0.6 ,  0.25,  0.6 ,  0.3 ,  0.45,  0.65,  0.3 ,
        0.15,  0.15,  0.2 ,  0.2 ,  0.95,  0.3 ,  0.3 ,  0.45,  0.4 ,
        ...,
        0.7 ,  0.6 ,  0.15,  0.2 ,  0.5 ,  0.45,  0.35])
   >>> row_means = np.mean(example_array, axis=1)
   >>> row_means
   array([ 0.41251326,  0.40509014,  0.39978791,  0.36267232,  0.41675504,
           0.40615058,  0.43160127,  0.40721103,  0.40296925,  0.37963945,
           0.38600212,  0.4019088 ,  0.39660657,  0.41887593,  0.41887593,
           0.39130435,  0.41675504,  0.40933192,  0.39342524,  0.41675504])

Similarly for calculating the standard deviation and variance.

.. code-block:: py
   :caption: Calculate standard deviation

   >>> column_stdev = np.std(example_array, axis=0)
   array([ 0.4330127 ,  0.3       ,  0.35707142,  0.21794495,  0.4330127 ,
        0.64031242,  0.4       ,  0.72629195,  0.78102497,  0.45825757,
        0.4330127 ,  0.76648549,  0.4       ,  0.58949131,  0.4       ,
        0.66895441,  0.21794495,  0.73993243,  0.3       ,  0.3       ,
        0.66332496,  0.4330127 ,  0.58309519,  0.55677644,  0.66895441,
        0.72629195,  0.45825757,  0.35707142,  0.35707142,  0.50990195,
        0.4       ,  0.58949131,  0.45825757,  0.45825757,  0.58949131,
        0.58309519,  0.64031242,  0.4       ,  0.53619026,  0.59160798,
        ...,
        0.45825757,  0.64031242,  0.48989795,  0.35707142,  0.4       ,
        0.59160798,  0.49749372,  0.4769696 ])
   >>> row_stdev = np.std(example_array, axis=1)
   array([ 0.57384893,  0.57081323,  0.56990628,  0.5583323 ,  0.60854229,
        0.58746569,  0.59987475,  0.60716032,  0.6029898 ,  0.5603448 ,
        0.57473792,  0.6028182 ,  0.58222991,  0.56548294,  0.60882866,
        0.57576423,  0.59443805,  0.61786575,  0.58529093,  0.59085939])

.. _operating_on_slices:

Operating on a Slice
^^^^^^^^^^^^^^^^^^^^

We can also do the same thing to a slice of the array.

.. code-block:: py
   :caption: Operating on slices

   >>> means_of_slice = np.mean(example_array[0:15:5, 100:800:50], axis=0)
   >>> means_of_slice
   array([ 0.33333333,  1.        ,  0.33333333,  0.33333333,  0.66666667,
           0.        ,  0.33333333,  0.        ,  0.33333333,  0.66666667,
           0.66666667,  0.        ,  0.33333333,  0.33333333])

.. _difference_between_array_and_matrix:

Differences Between Numpy Arrays and Numpy Matrices
===================================================

A Numpy array is not the same thing as a mathematical matrix. Multiplying two
Numpy arrays will multiply each corresponding element. Hence the arrays you are
multiplying must have matching dimensions. If you want a matrix product then you
have to first convert the arrays into a matrix using :py:func:`np.matrix`

.. code-block:: py
   :caption: Array multiplication

   >>> first_random = np.random.randn(20, 100)
   >>> second_random = np.random.randn(20, 100)
   >>> first_random * second_random
   array([[-0.036,  0.589,  0.63 , ..., -0.672,  0.309,  0.123],
       [ 0.846,  0.251, -0.919, ..., -0.002, -1.105,  1.998],
       [-0.702,  0.41 ,  0.059, ..., -0.093,  0.1  , -4.485],
       ...,
       [ 0.834,  1.113,  0.133, ..., -0.153, -0.945, -0.476],
       [-0.462,  0.418, -1.018, ..., -5.609, -0.227,  1.525],
       [ 0.2  ,  0.92 ,  0.463, ..., -0.201, -0.502,  1.902]])

Trying to multiply arrays which do not have the same dimension will result in an error.

.. code-block:: py
   :caption: Multiplying mismatched dimensions

   >>> mismatched_random = np.random.randn(10, 50)
   >>> first_random * mismatched_random
   ---------------------------------------------------------------------------
   ValueError                                Traceback (most recent call last)
   <ipython-input-64-f0ebbac38b11> in <module>()
   ----> 1 first_random * mismatched_random

   ValueError: operands could not be broadcast together with shapes (20,100) (10,50)

To do matrix multiplication first convert arrays to matrices. We can take the transpose
of an array or matrix by using its ``.T`` attribute similar to the ``.shape`` attribute.

.. code-block:: py
   :caption: Matrix multiplication

   >>> first_matrix = np.matrix(first_random)
   matrix([[-0.193, -0.913, -1.258, ...,  0.848, -0.942,  1.134],
           [ 0.536, -2.56 , -1.487, ...,  0.264, -1.504,  2.574],
           [ 0.978, -1.73 , -0.317, ..., -1.091,  0.541, -1.746],
           ...,
           [ 1.077,  1.623,  0.156, ...,  0.476, -1.198, -0.492],
           [ 0.225, -0.639, -0.571, ..., -2.204,  0.565, -1.086],
           [-0.148, -1.806,  0.596, ...,  0.874, -0.632,  1.047]])
   >>> second_matrix = np.matrix(first_random.T)
   >>> matrix_product = first_matrix * second_matrix
   >>> matrix_product
   matrix([[  6.269,  10.935,   9.302,   9.936,   1.838,  -6.395,   9.463,
          16.497,   1.65 ,   1.654,   8.431,  -0.437, -15.707,   5.318,
          19.149,   3.347,   5.519,  10.011,  -2.778,  -6.423],
        [ 11.177,   3.777,  22.378,   2.58 ,   6.941,   8.868, -13.875,
         -13.183,   6.825, -10.675,  -5.998,  -0.376, -13.904, -10.096,
          16.648,   3.82 ,  -2.35 ,  13.603,  -1.427,   7.775],
          ...
   >>> matrix_product.shape
   (20, 20)

.. _numpy_conclusion:

Summary of Numpy
================

Numpy is a vast module and very powerful. In general using numpy arrays will be
faster than using Python lists. However, you will lose some of the flexibility of Python. You
will probably see what I mean if you end up using Numpy in your work. See the
full Numpy documentation here_.

.. _here: http://docs.scipy.org/doc/numpy/reference/index.html

