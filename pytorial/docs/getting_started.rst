===============
Getting Started
===============

We will begin by starting the ``Python`` interpreter to make sure you have installed
everything properly and your computer is using the correct version of ``Python``.
Open the command line interface for your system:

   * Windows: :menuselection:`Start --> Git -->` :program:`Git Bash`
   * Mac: Search for Terminal. Open :program:`Terminal`
   * Linux: You probably already know how to open a terminal.

:Note: Whenever you see ``$`` in front of a code snippet it implies we are working in the shell

We are using the `Anaconda Distribution`_ so you
should see something like what follows in the example.

.. _Anaconda Distribution: https://www.continuum.io/downloads

.. code-block:: console

   $ python
   Python 3.5.1 | Contiuum Analytics, Inc. | (default, Feb 16 2016, 09:49:46) [MSVC v.1900 64 bit (AMD64) on] on win32
   Type "help", "copyright", "credits" or "license" for more information.

:Note: If you see something other than Python 3.5.x please put your red sticky on your laptop so we know to help you.

Honoring the Tradition
======================

It is tradition that your the first thing you do with a language is learn how to
print "Hello World!" to the screen. In line with that tradition go back to the Python
interpreter we just opened and type:

.. code-block:: python3

   >>> print("Hello World!")
   Hello World!

This is the plain old interactive ``Python`` interpreter. It can be difficult to
work with when dealing with even a modest amount of code. Luckily for us there
is an excellent enhancement to the regular interactive interpreter called IPython_
meaning "Interactive Python". Even better than ``IPython`` is the `Jupyter Notebook`_

:Note: The IPython has recently renamed itself to Jupyter.

.. _IPython: https://ipython.org/
.. _Jupyter Notebook: http://jupyter.org/

We will be using the ``Jupyter Notebook`` for most of our work. We strongly
encourage you to continue using the ``Jupyter Notebook`` in your own work. It
eases some of the pain of writing code with features such as tab-completion and cell-by-cell
evaluation. First quit out of the regular ``Python`` interpreter by running:

.. code-block:: python

   >>> quit()

.. _git_bash_benefits:

Windows
~~~~~~~

``Windows`` has two terminals installed: **cmd** and **PowerShell**. Both of these
terminals are difficult to use and lack many of the excellent features of the Linux
``bash`` terminal. Fortunately ``Git Bash`` emulates ``bash`` very well. So all
the commands we show in the shell can be executed in ``Git Bash``. In my own case
I was forced to learn how to work with ``PowerShell`` for my local computer and
``bash`` when I did work on the cluster. The new and improved ``Git Bash`` allows us
to use ``bash`` on a Windows machine.