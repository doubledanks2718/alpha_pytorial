
.. _using_pandas:

============
Using Pandas
============

pandas_ is a high level package with user-friendly data structures
and analysis tools. This tutorial will show you the basics of using
pandas_ to read/write data and to navigate its data structures such as
:class:`DataFrame`. As usual we will be working in the ``Jupyter Notebook``. There
is a :mod:`numpy` array *underneath* every :class:`DataFrame`.

.. _pandas: http://pandas.pydata.org

.. _reading_and_writing_files:

Reading and Writing Files
=========================

:mod:`pandas` provides:py funcs to read and write a variety of file formats: http://pandas.pydata.org/pandas-docs/stable/io.html
For the foreseeable future we will be working with ``csv`` files i.e. we will
be using the :py:func:`read_csv` to read files and :py:func:`to_csv` to write files.
The sample data we will be working with is located in ``Dropbox/summer_statistics_fun``

Local Copy and Jupyter Notebook
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Make a local copy of the Dropbox files. Copy the files into the folder you would
like to work with for example:

   ``C:\Users\DoubleDanks\working``

Then open a terminal and navigate to the folder with your files using the
``cd`` command.

.. code-block:: console
   :caption: Navigate to working folder

   $ pwd
   C:\Users\DoubleDanks
   $ cd C:\Users\DoubleDanks\working
   $ pwd
   C:\Users\DoubleDanks\working

Make sure that our files are in our folder using the ``ls`` command

.. code-block:: console
   :caption: Using ``ls``

   $ ls
   example_1.txt
   example_estimated_vs_actual_allele_effects.txt
   example_mean_and_stdev_power_fpr.txt
   example_power_fpr_results.txt

Then we start up our ``Jupyter Notebook`` as usual.

.. code-block:: console
   :caption: Start the ``Jupyter Notebook``

   $ jupyter notebook
   [I 17:00:00.622 NotebookApp] Serving notebooks from local directory: C:\Users\DoubleDanks\working
   [I 17:00:00.622 NotebookApp] 0 active kernels
   [I 17:00:00.623 NotebookApp] The Jupyter Notebook is running at: http://localhost:8890/
   [I 17:00:00.625 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).

.. note::

   The port your notebook uses might be different. Mine is running on 8890 because I have
   two other notebooks open at the time of writing.

You should have something that looks like this:

.. image:: images/jupyter_notebook_2.PNG

Reading a File
^^^^^^^^^^^^^^

Reading a file with a delimiter such as a tab character ``\t`` or a comma ``,``
is very simple in pandas. We will use the :py:func:`read_csv` which requires
a string filename.

.. code-block:: py
   :caption: First attempt to read tab-delimited file

   >>> df = pd.read_csv('example_1.txt', sep='\t')
   >>> df

.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Chr	df	F	p	add_effect	add_F	add_p	dom_effect	dom_F	dom_p	q	minor_allele	minor_frequency	major_allele	major_frequency	locus	alpha_allele	alpha_frequency	alpha_effect	beta_allele	beta_frequency	beta_effect	difference</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0\t1.0\t2\t0.31633\t0.7288899999999999\t-0.132...</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1\t1.0\t2\t0.37051999999999996\t0.69047\t0.034...</td>
        </tr>
        <tr>
          <th>2</th>
          <td>2\t1.0\t2\t0.8942399999999999\t0.40925\t0.1675...</td>
        </tr>
        <tr>
          <th>3</th>
          <td>3\t1.0\t2\t0.17267000000000002\t0.84144\t0.059...</td>
        </tr>
        <tr>
          <th>940</th>
          <td>940\t10.0\t2\t0.20595\t0.81391\t-0.083504\t0.3...</td>
        </tr>
        <tr>
          <th>941</th>
          <td>941\t10.0\t2\t0.37246\t0.68913\t-0.098097\t0.4...</td>
        </tr>
        <tr>
          <th>942</th>
          <td>942\t10.0\t2\t0.18878\t0.828\t0.12424\t0.35072...</td>
        </tr>
      </tbody>
    </table>
    <p>943 rows × 1 columns</p>
    </div>

Hmmm this does not quite look like what we want. We have to specify the the separating
character using the ``sep`` keyword argument.

.. code-block:: py
   :caption: Read tab-delimited file

   >>> import pandas as pd
   >>> df = pd.read_csv('example_1.txt', sep='\t')
   >>> df

.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Unnamed: 0</th>
          <th>Chr</th>
          <th>df</th>
          <th>F</th>
          <th>p</th>
          <th>add_effect</th>
          <th>add_F</th>
          <th>add_p</th>
          <th>dom_effect</th>
          <th>dom_F</th>
          <th>...</th>
          <th>major_allele</th>
          <th>major_frequency</th>
          <th>locus</th>
          <th>alpha_allele</th>
          <th>alpha_frequency</th>
          <th>alpha_effect</th>
          <th>beta_allele</th>
          <th>beta_frequency</th>
          <th>beta_effect</th>
          <th>difference</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0</td>
          <td>1.0</td>
          <td>2</td>
          <td>0.31633</td>
          <td>7.288900e-01</td>
          <td>-0.132000</td>
          <td>0.523020</td>
          <td>4.697300e-01</td>
          <td>-0.038743</td>
          <td>0.04852</td>
          <td>...</td>
          <td>1.0</td>
          <td>0.8850</td>
          <td>1</td>
          <td>1</td>
          <td>0.8850</td>
          <td>0.000000</td>
          <td>3</td>
          <td>0.1150</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1</td>
          <td>1.0</td>
          <td>2</td>
          <td>0.37052</td>
          <td>6.904700e-01</td>
          <td>0.034310</td>
          <td>0.044760</td>
          <td>8.324900e-01</td>
          <td>-0.121500</td>
          <td>0.62439</td>
          <td>...</td>
          <td>3.0</td>
          <td>0.8645</td>
          <td>2</td>
          <td>3</td>
          <td>0.8645</td>
          <td>0.000000</td>
          <td>1</td>
          <td>0.1355</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>2</th>
          <td>2</td>
          <td>1.0</td>
          <td>2</td>
          <td>0.89424</td>
          <td>4.092500e-01</td>
          <td>0.167540</td>
          <td>0.814020</td>
          <td>3.671500e-01</td>
          <td>-0.024772</td>
          <td>0.02053</td>
          <td>...</td>
          <td>0.0</td>
          <td>0.8690</td>
          <td>3</td>
          <td>0</td>
          <td>0.8690</td>
          <td>0.000000</td>
          <td>2</td>
          <td>0.1310</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>3</th>
          <td>3</td>
          <td>1.0</td>
          <td>2</td>
          <td>0.17267</td>
          <td>8.414400e-01</td>
          <td>0.059300</td>
          <td>0.037270</td>
          <td>8.469500e-01</td>
          <td>0.044820</td>
          <td>0.02077</td>
          <td>...</td>
          <td>2.0</td>
          <td>0.9490</td>
          <td>4</td>
          <td>2</td>
          <td>0.9490</td>
          <td>0.000000</td>
          <td>0</td>
          <td>0.0510</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>940</th>
          <td>940</td>
          <td>10.0</td>
          <td>2</td>
          <td>0.20595</td>
          <td>8.139100e-01</td>
          <td>-0.083504</td>
          <td>0.368770</td>
          <td>5.438100e-01</td>
          <td>-0.008490</td>
          <td>0.00608</td>
          <td>...</td>
          <td>0.0</td>
          <td>0.7395</td>
          <td>1474</td>
          <td>0</td>
          <td>0.7395</td>
          <td>0.000000</td>
          <td>2</td>
          <td>0.2605</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>941</th>
          <td>941</td>
          <td>10.0</td>
          <td>2</td>
          <td>0.37246</td>
          <td>6.891300e-01</td>
          <td>-0.098097</td>
          <td>0.465380</td>
          <td>4.952800e-01</td>
          <td>0.093480</td>
          <td>0.63779</td>
          <td>...</td>
          <td>1.0</td>
          <td>0.7690</td>
          <td>1475</td>
          <td>1</td>
          <td>0.7690</td>
          <td>0.000000</td>
          <td>0</td>
          <td>0.2310</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>942</th>
          <td>942</td>
          <td>10.0</td>
          <td>2</td>
          <td>0.18878</td>
          <td>8.280000e-01</td>
          <td>0.124240</td>
          <td>0.350720</td>
          <td>5.538400e-01</td>
          <td>-0.056447</td>
          <td>0.09327</td>
          <td>...</td>
          <td>3.0</td>
          <td>0.8670</td>
          <td>1476</td>
          <td>3</td>
          <td>0.8670</td>
          <td>0.000000</td>
          <td>0</td>
          <td>0.1330</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
      </tbody>
    </table>
    <p>943 rows × 24 columns</p>
    </div>


Much better but not quite right.
It seems we have an unwanted column of ``Unnamed: 0``. It is because when I saved
this file I saved it using the index. We can get rid of this column by using another
keyword argument called ``index_col``.

.. code-block:: py
   :caption: Read tab-delimited file using first column as index

   >>> df = pd.read_csv('example_1.txt', sep='\t', index_col=0)
   >>> df

.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Chr</th>
          <th>df</th>
          <th>F</th>
          <th>p</th>
          <th>add_effect</th>
          <th>add_F</th>
          <th>add_p</th>
          <th>dom_effect</th>
          <th>dom_F</th>
          <th>dom_p</th>
          <th>...</th>
          <th>major_allele</th>
          <th>major_frequency</th>
          <th>locus</th>
          <th>alpha_allele</th>
          <th>alpha_frequency</th>
          <th>alpha_effect</th>
          <th>beta_allele</th>
          <th>beta_frequency</th>
          <th>beta_effect</th>
          <th>difference</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.31633</td>
          <td>7.288900e-01</td>
          <td>-0.132000</td>
          <td>0.523020</td>
          <td>4.697300e-01</td>
          <td>-0.038743</td>
          <td>0.04852</td>
          <td>0.82571</td>
          <td>...</td>
          <td>1.0</td>
          <td>0.8850</td>
          <td>1</td>
          <td>1</td>
          <td>0.8850</td>
          <td>0.000000</td>
          <td>3</td>
          <td>0.1150</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.37052</td>
          <td>6.904700e-01</td>
          <td>0.034310</td>
          <td>0.044760</td>
          <td>8.324900e-01</td>
          <td>-0.121500</td>
          <td>0.62439</td>
          <td>0.42961</td>
          <td>...</td>
          <td>3.0</td>
          <td>0.8645</td>
          <td>2</td>
          <td>3</td>
          <td>0.8645</td>
          <td>0.000000</td>
          <td>1</td>
          <td>0.1355</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.89424</td>
          <td>4.092500e-01</td>
          <td>0.167540</td>
          <td>0.814020</td>
          <td>3.671500e-01</td>
          <td>-0.024772</td>
          <td>0.02053</td>
          <td>0.88609</td>
          <td>...</td>
          <td>0.0</td>
          <td>0.8690</td>
          <td>3</td>
          <td>0</td>
          <td>0.8690</td>
          <td>0.000000</td>
          <td>2</td>
          <td>0.1310</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.17267</td>
          <td>8.414400e-01</td>
          <td>0.059300</td>
          <td>0.037270</td>
          <td>8.469500e-01</td>
          <td>0.044820</td>
          <td>0.02077</td>
          <td>0.88543</td>
          <td>...</td>
          <td>2.0</td>
          <td>0.9490</td>
          <td>4</td>
          <td>2</td>
          <td>0.9490</td>
          <td>0.000000</td>
          <td>0</td>
          <td>0.0510</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>940</th>
          <td>10.0</td>
          <td>2</td>
          <td>0.20595</td>
          <td>8.139100e-01</td>
          <td>-0.083504</td>
          <td>0.368770</td>
          <td>5.438100e-01</td>
          <td>-0.008490</td>
          <td>0.00608</td>
          <td>0.93786</td>
          <td>...</td>
          <td>0.0</td>
          <td>0.7395</td>
          <td>1474</td>
          <td>0</td>
          <td>0.7395</td>
          <td>0.000000</td>
          <td>2</td>
          <td>0.2605</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>941</th>
          <td>10.0</td>
          <td>2</td>
          <td>0.37246</td>
          <td>6.891300e-01</td>
          <td>-0.098097</td>
          <td>0.465380</td>
          <td>4.952800e-01</td>
          <td>0.093480</td>
          <td>0.63779</td>
          <td>0.42470</td>
          <td>...</td>
          <td>1.0</td>
          <td>0.7690</td>
          <td>1475</td>
          <td>1</td>
          <td>0.7690</td>
          <td>0.000000</td>
          <td>0</td>
          <td>0.2310</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>942</th>
          <td>10.0</td>
          <td>2</td>
          <td>0.18878</td>
          <td>8.280000e-01</td>
          <td>0.124240</td>
          <td>0.350720</td>
          <td>5.538400e-01</td>
          <td>-0.056447</td>
          <td>0.09327</td>
          <td>0.76012</td>
          <td>...</td>
          <td>3.0</td>
          <td>0.8670</td>
          <td>1476</td>
          <td>3</td>
          <td>0.8670</td>
          <td>0.000000</td>
          <td>0</td>
          <td>0.1330</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
      </tbody>
    </table>
    <p>943 rows × 23 columns</p>
    </div>



Much prettier. No more ``Unnamed:0`` column.


Writing a File
^^^^^^^^^^^^^^

Writing a file is just as easy in :mod:`pandas`. We use the :py:func:`to_csv`:py func
with nearly identical arguments. Let's save ``df`` to a different file name.

.. code-block:: py
   :caption: Writing a tab-delimited file

   >>> df.to_csv('new_file_name.txt', sep='\t')

If you look in your directory you should see the newly written file.

.. _summary_of_read_write:

Summary
^^^^^^^

In summary we copied the files from the Dropbox, navigated to their location using
the terminal and then we started the ``Jupyter Notebook``. Reading and writing files
using :mod:`pandas` is very simple and supports a `wide variety of formats`_

.. _`wide variety of formats`: http://pandas.pydata.org/pandas-docs/stable/io.html


Navigating :class:`DataFrame`\s
===============================

Working with a :class:`DataFrame` requires us to remember a couple functions. For
lists and Numpy :class:`array`s we can simple use integer indexing; however,
a :class:`DataFrame` requires us to use ``.loc``, ``.iloc`` or ``.ix``.

+ :py:func:`loc` is for cases where you ``index`` are strings. If your ``index`` consists of integers then ``loc`` and ``iloc`` are equivalent
+ :py:func:`iloc` can be used with integer based indexes.
+ :py:func:`ix` supports 2D indexing as with Numpy arrays.

Just like Numpy the :class:`array` and Python :class:`list` we can slice a
:class:`DataFrame` to get a smaller :class:`DataFrame`.

.. note::

   A single row of a :class:`DataFrame` is a :class:`Series`. Multiple
   :class:`DataFrame`\s is a :class:`Panel`.

.. code-block:: py
   :caption: Introduction to accessing :class:`DataFrame` elements.

   >>> df.loc[0]
   Chr                1.000000
   df                 2.000000
   F                  0.316330
   p                  0.728890
   add_effect        -0.132000
   add_F              0.523020
   add_p              0.469730
   dom_effect        -0.038743
   dom_F              0.048520
   dom_p              0.825710
   q                  0.999610
   minor_allele       3.000000
   minor_frequency    0.115000
   major_allele       1.000000
   major_frequency    0.885000
   locus              1.000000
   alpha_allele       1.000000
   alpha_frequency    0.885000
   alpha_effect       0.000000
   beta_allele        3.000000
   beta_frequency     0.115000
   beta_effect        0.000000
   difference         0.000000
   Name: 0, dtype: float64
   >>> df.iloc[0]
   Chr                1.000000
   df                 2.000000
   F                  0.316330
   p                  0.728890
   add_effect        -0.132000
   add_F              0.523020
   add_p              0.469730
   dom_effect        -0.038743
   dom_F              0.048520
   dom_p              0.825710
   q                  0.999610
   minor_allele       3.000000
   minor_frequency    0.115000
   major_allele       1.000000
   major_frequency    0.885000
   locus              1.000000
   alpha_allele       1.000000
   alpha_frequency    0.885000
   alpha_effect       0.000000
   beta_allele        3.000000
   beta_frequency     0.115000
   beta_effect        0.000000
   difference         0.000000
   Name: 0, dtype: float64
   >>> df.loc[0] == df.iloc[0] # elementwise comparison
   Chr                True
   df                 True
   F                  True
   p                  True
   add_effect         True
   add_F              True
   add_p              True
   dom_effect         True
   dom_F              True
   dom_p              True
   q                  True
   minor_allele       True
   minor_frequency    True
   major_allele       True
   major_frequency    True
   locus              True
   alpha_allele       True
   alpha_frequency    True
   alpha_effect       True
   beta_allele        True
   beta_frequency     True
   beta_effect        True
   difference         True
   Name: 0, dtype: bool

.. _using_ix:

I prefer to use ``.ix`` for accessing elements because it is most similar to :mod:`numpy`.
``.ix`` returns a :py:class:`DataFrame` and not a :py:class:`Series`. We will learn
how to use ``.ix`` to slice rows and columns. We will also learn about the somewhat
strange syntax to use ``.ix`` to take subsets based upon the value of a column.

.. code-block:: py
   :caption: Using ``.ix`` to slice on rows

   >>> df.ix[1:10, :] # rows 1, 2, 3, 4, 5, 6, 7, 8, 9, 10

.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Chr</th>
          <th>df</th>
          <th>F</th>
          <th>p</th>
          <th>add_effect</th>
          <th>add_F</th>
          <th>add_p</th>
          <th>dom_effect</th>
          <th>dom_F</th>
          <th>dom_p</th>
          <th>...</th>
          <th>major_allele</th>
          <th>major_frequency</th>
          <th>locus</th>
          <th>alpha_allele</th>
          <th>alpha_frequency</th>
          <th>alpha_effect</th>
          <th>beta_allele</th>
          <th>beta_frequency</th>
          <th>beta_effect</th>
          <th>difference</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.37052</td>
          <td>0.69047</td>
          <td>0.034310</td>
          <td>0.04476</td>
          <td>0.83249</td>
          <td>-0.121500</td>
          <td>0.62439</td>
          <td>0.42961</td>
          <td>...</td>
          <td>3.0</td>
          <td>0.8645</td>
          <td>2</td>
          <td>3</td>
          <td>0.8645</td>
          <td>0.0</td>
          <td>1</td>
          <td>0.1355</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.89424</td>
          <td>0.40925</td>
          <td>0.167540</td>
          <td>0.81402</td>
          <td>0.36715</td>
          <td>-0.024772</td>
          <td>0.02053</td>
          <td>0.88609</td>
          <td>...</td>
          <td>0.0</td>
          <td>0.8690</td>
          <td>3</td>
          <td>0</td>
          <td>0.8690</td>
          <td>0.0</td>
          <td>2</td>
          <td>0.1310</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.17267</td>
          <td>0.84144</td>
          <td>0.059300</td>
          <td>0.03727</td>
          <td>0.84695</td>
          <td>0.044820</td>
          <td>0.02077</td>
          <td>0.88543</td>
          <td>...</td>
          <td>2.0</td>
          <td>0.9490</td>
          <td>4</td>
          <td>2</td>
          <td>0.9490</td>
          <td>0.0</td>
          <td>0</td>
          <td>0.0510</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1.0</td>
          <td>2</td>
          <td>2.36851</td>
          <td>0.09415</td>
          <td>0.201590</td>
          <td>1.75893</td>
          <td>0.18506</td>
          <td>0.257570</td>
          <td>4.64273</td>
          <td>0.03142</td>
          <td>...</td>
          <td>0.0</td>
          <td>0.7760</td>
          <td>5</td>
          <td>0</td>
          <td>0.7760</td>
          <td>0.0</td>
          <td>2</td>
          <td>0.2240</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>5</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.73828</td>
          <td>0.47820</td>
          <td>-0.156370</td>
          <td>1.30226</td>
          <td>0.25408</td>
          <td>-0.088060</td>
          <td>0.69045</td>
          <td>0.40621</td>
          <td>...</td>
          <td>0.0</td>
          <td>0.7410</td>
          <td>6</td>
          <td>0</td>
          <td>0.7410</td>
          <td>0.0</td>
          <td>2</td>
          <td>0.2590</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>6</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.03184</td>
          <td>0.96867</td>
          <td>-0.014967</td>
          <td>0.00419</td>
          <td>0.94839</td>
          <td>0.024620</td>
          <td>0.01348</td>
          <td>0.90761</td>
          <td>...</td>
          <td>0.0</td>
          <td>0.8865</td>
          <td>8</td>
          <td>0</td>
          <td>0.8865</td>
          <td>0.0</td>
          <td>2</td>
          <td>0.1135</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>7</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.97999</td>
          <td>0.37568</td>
          <td>0.172770</td>
          <td>1.81634</td>
          <td>0.17806</td>
          <td>0.045730</td>
          <td>0.30871</td>
          <td>0.57860</td>
          <td>...</td>
          <td>1.0</td>
          <td>0.6035</td>
          <td>10</td>
          <td>1</td>
          <td>0.6035</td>
          <td>0.0</td>
          <td>3</td>
          <td>0.3965</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>8</th>
          <td>1.0</td>
          <td>2</td>
          <td>1.22845</td>
          <td>0.29319</td>
          <td>-0.010055</td>
          <td>0.00616</td>
          <td>0.93744</td>
          <td>-0.137150</td>
          <td>2.21424</td>
          <td>0.13706</td>
          <td>...</td>
          <td>3.0</td>
          <td>0.6790</td>
          <td>12</td>
          <td>3</td>
          <td>0.6790</td>
          <td>0.0</td>
          <td>1</td>
          <td>0.3210</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
        <tr>
          <th>9</th>
          <td>1.0</td>
          <td>2</td>
          <td>1.37046</td>
          <td>0.25447</td>
          <td>-0.256970</td>
          <td>1.45020</td>
          <td>0.22878</td>
          <td>0.324970</td>
          <td>2.74017</td>
          <td>0.09817</td>
          <td>...</td>
          <td>2.0</td>
          <td>0.8795</td>
          <td>13</td>
          <td>2</td>
          <td>0.8795</td>
          <td>0.0</td>
          <td>0</td>
          <td>0.1205</td>
          <td>0.0</td>
          <td>0.0</td>
        </tr>
      </tbody>
    </table>
    <p>9 rows × 23 columns</p>
    </div>


We can also slice on columns even though they are strings.

.. code-block:: py
   :caption: Row and column slice

   >>> df.ix[1:10, 'Chr':'add_p']


.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Chr</th>
          <th>df</th>
          <th>F</th>
          <th>p</th>
          <th>add_effect</th>
          <th>add_F</th>
          <th>add_p</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.37052</td>
          <td>0.69047</td>
          <td>0.034310</td>
          <td>0.04476</td>
          <td>0.83249</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.89424</td>
          <td>0.40925</td>
          <td>0.167540</td>
          <td>0.81402</td>
          <td>0.36715</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.17267</td>
          <td>0.84144</td>
          <td>0.059300</td>
          <td>0.03727</td>
          <td>0.84695</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1.0</td>
          <td>2</td>
          <td>2.36851</td>
          <td>0.09415</td>
          <td>0.201590</td>
          <td>1.75893</td>
          <td>0.18506</td>
        </tr>
        <tr>
          <th>5</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.73828</td>
          <td>0.47820</td>
          <td>-0.156370</td>
          <td>1.30226</td>
          <td>0.25408</td>
        </tr>
        <tr>
          <th>6</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.03184</td>
          <td>0.96867</td>
          <td>-0.014967</td>
          <td>0.00419</td>
          <td>0.94839</td>
        </tr>
        <tr>
          <th>7</th>
          <td>1.0</td>
          <td>2</td>
          <td>0.97999</td>
          <td>0.37568</td>
          <td>0.172770</td>
          <td>1.81634</td>
          <td>0.17806</td>
        </tr>
        <tr>
          <th>8</th>
          <td>1.0</td>
          <td>2</td>
          <td>1.22845</td>
          <td>0.29319</td>
          <td>-0.010055</td>
          <td>0.00616</td>
          <td>0.93744</td>
        </tr>
        <tr>
          <th>9</th>
          <td>1.0</td>
          <td>2</td>
          <td>1.37046</td>
          <td>0.25447</td>
          <td>-0.256970</td>
          <td>1.45020</td>
          <td>0.22878</td>
        </tr>
        <tr>
          <th>10</th>
          <td>1.0</td>
          <td>2</td>
          <td>1.06546</td>
          <td>0.34496</td>
          <td>-0.326950</td>
          <td>2.08576</td>
          <td>0.14899</td>
        </tr>
      </tbody>
    </table>
    </div>

Creating a New :py:class:`DataFrame`
====================================

We have talked about putting data in a file into a :py:class:`DataFrame` but we
have not actually created one from existing data.

.. _creating_from_existing_data:

.. code-block:: py
   :caption: Make a new :py:class:`DataFrame`

   >>> import numpy as np
   >>> random_array = np.random.randn(100, 100)
   >>> new_dataframe = pd.DataFrame(random_array)
   >>> new_dataframe

We can change the labels of the rows and columns by accessing the ``.index`` and ``.columns`` attribute.

   >>> new_dataframe.columns # current column labels
   RangeIndex(start=0, stop=100, step=1)
   >>> new_dataframe.columns = ['col' + str(i) for i in range(len(new_dataframe.columns))]
   >>> new_dataframe.columns
   Index(['row0', 'row1', 'row2', 'row3', 'row4', 'row5', 'row6', 'row7', 'row8',
          'row9', 'row10', 'row11', 'row12', 'row13', 'row14', 'row15', 'row16',
          'row17', 'row18', 'row19', 'row20', 'row21', 'row22', 'row23', 'row24',
          'row25', 'row26', 'row27', 'row28', 'row29', 'row30', 'row31', 'row32',
          'row33', 'row34', 'row35', 'row36', 'row37', 'row38', 'row39', 'row40',
          'row41', 'row42', 'row43', 'row44', 'row45', 'row46', 'row47', 'row48',
          'row49', 'row50', 'row51', 'row52', 'row53', 'row54', 'row55', 'row56',
          'row57', 'row58', 'row59', 'row60', 'row61', 'row62', 'row63', 'row64',
          'row65', 'row66', 'row67', 'row68', 'row69', 'row70', 'row71', 'row72',
          'row73', 'row74', 'row75', 'row76', 'row77', 'row78', 'row79', 'row80',
          'row81', 'row82', 'row83', 'row84', 'row85', 'row86', 'row87', 'row88',
          'row89', 'row90', 'row91', 'row92', 'row93', 'row94', 'row95', 'row96',
          'row97', 'row98', 'row99'],
         dtype='object')
   >>> new_dataframe.index = ['row'+str(i) for i in range(len(new_dataframe.index))]
   >>> new_dataframe.ix[0:10, 0:10]

.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>col0</th>
          <th>col1</th>
          <th>col2</th>
          <th>col3</th>
          <th>col4</th>
          <th>col5</th>
          <th>col6</th>
          <th>col7</th>
          <th>col8</th>
          <th>col9</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>row0</th>
          <td>0.851769</td>
          <td>0.552416</td>
          <td>-1.138647</td>
          <td>0.255327</td>
          <td>-0.007743</td>
          <td>1.089647</td>
          <td>-0.778752</td>
          <td>-0.649297</td>
          <td>0.143723</td>
          <td>-0.970807</td>
        </tr>
        <tr>
          <th>row1</th>
          <td>-1.003297</td>
          <td>-1.744152</td>
          <td>-0.426269</td>
          <td>-1.669010</td>
          <td>-0.328639</td>
          <td>-0.464497</td>
          <td>0.950282</td>
          <td>1.588335</td>
          <td>-0.228244</td>
          <td>1.762874</td>
        </tr>
        <tr>
          <th>row2</th>
          <td>-0.541226</td>
          <td>-0.003163</td>
          <td>-0.993038</td>
          <td>-0.654439</td>
          <td>-1.957676</td>
          <td>-1.434473</td>
          <td>0.478004</td>
          <td>-1.717364</td>
          <td>-0.820456</td>
          <td>-1.679173</td>
        </tr>
        <tr>
          <th>row3</th>
          <td>-1.571887</td>
          <td>-0.219255</td>
          <td>-0.918732</td>
          <td>0.866438</td>
          <td>-0.661378</td>
          <td>0.585361</td>
          <td>-1.281573</td>
          <td>1.278456</td>
          <td>0.456524</td>
          <td>0.567753</td>
        </tr>
        <tr>
          <th>row4</th>
          <td>0.361910</td>
          <td>1.575642</td>
          <td>-0.886834</td>
          <td>-0.948297</td>
          <td>-0.596473</td>
          <td>0.582711</td>
          <td>0.519419</td>
          <td>0.072056</td>
          <td>0.094672</td>
          <td>1.100558</td>
        </tr>
        <tr>
          <th>row5</th>
          <td>0.997240</td>
          <td>-0.390215</td>
          <td>-0.970183</td>
          <td>0.013025</td>
          <td>0.991526</td>
          <td>-0.581896</td>
          <td>1.338808</td>
          <td>-1.613876</td>
          <td>0.246216</td>
          <td>1.519554</td>
        </tr>
        <tr>
          <th>row6</th>
          <td>0.142314</td>
          <td>1.026342</td>
          <td>-2.073610</td>
          <td>-0.300846</td>
          <td>0.980148</td>
          <td>0.427390</td>
          <td>0.872479</td>
          <td>0.340115</td>
          <td>-0.010224</td>
          <td>0.936614</td>
        </tr>
        <tr>
          <th>row7</th>
          <td>1.713222</td>
          <td>-0.163328</td>
          <td>-0.722491</td>
          <td>0.629925</td>
          <td>-0.669115</td>
          <td>-0.873051</td>
          <td>0.074855</td>
          <td>-2.648575</td>
          <td>-1.887346</td>
          <td>-0.565827</td>
        </tr>
        <tr>
          <th>row8</th>
          <td>0.757490</td>
          <td>2.746054</td>
          <td>-0.376674</td>
          <td>-0.305580</td>
          <td>-1.671122</td>
          <td>-0.492839</td>
          <td>-0.346100</td>
          <td>-0.408035</td>
          <td>-0.516232</td>
          <td>-2.213593</td>
        </tr>
        <tr>
          <th>row9</th>
          <td>1.479184</td>
          <td>-1.428917</td>
          <td>-0.837945</td>
          <td>0.671179</td>
          <td>0.587842</td>
          <td>-1.541868</td>
          <td>0.403671</td>
          <td>-0.294927</td>
          <td>0.609008</td>
          <td>-0.429398</td>
        </tr>
      </tbody>
    </table>
    </div>

We can also use ``.ix`` to select a subset of the data based on some condition. The
syntax is a bit strange. We will get all the rows and the first five columns
from ``df`` which have p-values lower than 0.05.

.. code-block:: py
   :caption: Subsetting based upon some condition

   >>> df.ix[df.ix[:, 'p'] < 0.05, :5]

.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Chr</th>
          <th>df</th>
          <th>F</th>
          <th>p</th>
          <th>add_effect</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>16</th>
          <td>1.0</td>
          <td>2</td>
          <td>5.55007</td>
          <td>4.010000e-03</td>
          <td>-1.278100</td>
        </tr>
        <tr>
          <th>27</th>
          <td>1.0</td>
          <td>2</td>
          <td>64.13061</td>
          <td>6.350400e-27</td>
          <td>1.924560</td>
        </tr>
        <tr>
          <th>29</th>
          <td>1.0</td>
          <td>2</td>
          <td>7.85226</td>
          <td>4.134700e-04</td>
          <td>0.726020</td>
        </tr>
        <tr>
          <th>53</th>
          <td>1.0</td>
          <td>2</td>
          <td>3.19703</td>
          <td>4.130000e-02</td>
          <td>0.093530</td>
        </tr>
        <tr>
          <th>167</th>
          <td>2.0</td>
          <td>2</td>
          <td>4.41457</td>
          <td>1.234000e-02</td>
          <td>-1.865200</td>
        </tr>
        <tr>
          <th>199</th>
          <td>2.0</td>
          <td>2</td>
          <td>5.63324</td>
          <td>3.690000e-03</td>
          <td>0.310820</td>
        </tr>
        <tr>
          <th>225</th>
          <td>2.0</td>
          <td>2</td>
          <td>4.55773</td>
          <td>1.071000e-02</td>
          <td>-0.521010</td>
        </tr>
        <tr>
          <th>231</th>
          <td>3.0</td>
          <td>2</td>
          <td>7.77863</td>
          <td>4.445500e-04</td>
          <td>-0.795050</td>
        </tr>
        <tr>
          <th>241</th>
          <td>3.0</td>
          <td>2</td>
          <td>12.14438</td>
          <td>6.153300e-06</td>
          <td>-0.679950</td>
        </tr>
        <tr>
          <th>243</th>
          <td>3.0</td>
          <td>2</td>
          <td>136.63303</td>
          <td>3.713800e-53</td>
          <td>-2.565800</td>
        </tr>
        <tr>
          <th>244</th>
          <td>3.0</td>
          <td>2</td>
          <td>3.59209</td>
          <td>2.790000e-02</td>
          <td>0.389350</td>
        </tr>
        <tr>
          <th>245</th>
          <td>3.0</td>
          <td>2</td>
          <td>7.77550</td>
          <td>4.459200e-04</td>
          <td>-0.527940</td>
        </tr>
        <tr>
          <th>246</th>
          <td>3.0</td>
          <td>2</td>
          <td>5.14445</td>
          <td>5.990000e-03</td>
          <td>-0.417560</td>
        </tr>
        <tr>
          <th>249</th>
          <td>3.0</td>
          <td>2</td>
          <td>9.64770</td>
          <td>7.082200e-05</td>
          <td>-0.602540</td>
        </tr>
        <tr>
          <th>254</th>
          <td>3.0</td>
          <td>2</td>
          <td>4.27628</td>
          <td>1.415000e-02</td>
          <td>0.509890</td>
        </tr>
        <tr>
          <th>297</th>
          <td>3.0</td>
          <td>2</td>
          <td>7.61282</td>
          <td>5.234000e-04</td>
          <td>-0.471570</td>
        </tr>
        <tr>
          <th>347</th>
          <td>4.0</td>
          <td>2</td>
          <td>3.04674</td>
          <td>4.796000e-02</td>
          <td>-0.301630</td>
        </tr>
        <tr>
          <th>363</th>
          <td>4.0</td>
          <td>2</td>
          <td>4.19034</td>
          <td>1.541000e-02</td>
          <td>-0.393250</td>
        </tr>
        <tr>
          <th>364</th>
          <td>4.0</td>
          <td>2</td>
          <td>7.50622</td>
          <td>5.813500e-04</td>
          <td>-0.569120</td>
        </tr>
        <tr>
          <th>366</th>
          <td>4.0</td>
          <td>2</td>
          <td>7.51360</td>
          <td>5.771400e-04</td>
          <td>-0.545670</td>
        </tr>
        <tr>
          <th>372</th>
          <td>4.0</td>
          <td>2</td>
          <td>3.83687</td>
          <td>2.188000e-02</td>
          <td>0.275610</td>
        </tr>
        <tr>
          <th>374</th>
          <td>4.0</td>
          <td>2</td>
          <td>37.85950</td>
          <td>1.423700e-16</td>
          <td>-1.627500</td>
        </tr>
        <tr>
          <th>502</th>
          <td>5.0</td>
          <td>2</td>
          <td>3.62203</td>
          <td>2.708000e-02</td>
          <td>0.577740</td>
        </tr>
        <tr>
          <th>505</th>
          <td>5.0</td>
          <td>2</td>
          <td>3.75154</td>
          <td>2.381000e-02</td>
          <td>-0.498810</td>
        </tr>
        <tr>
          <th>506</th>
          <td>5.0</td>
          <td>2</td>
          <td>7.26058</td>
          <td>7.405500e-04</td>
          <td>0.183580</td>
        </tr>
        <tr>
          <th>546</th>
          <td>6.0</td>
          <td>2</td>
          <td>5.75104</td>
          <td>3.290000e-03</td>
          <td>-0.280710</td>
        </tr>
        <tr>
          <th>553</th>
          <td>6.0</td>
          <td>2</td>
          <td>3.02076</td>
          <td>4.921000e-02</td>
          <td>-0.056355</td>
        </tr>
        <tr>
          <th>587</th>
          <td>6.0</td>
          <td>2</td>
          <td>3.12512</td>
          <td>4.436000e-02</td>
          <td>-0.033469</td>
        </tr>
        <tr>
          <th>663</th>
          <td>7.0</td>
          <td>2</td>
          <td>4.61881</td>
          <td>1.008000e-02</td>
          <td>-0.364360</td>
        </tr>
        <tr>
          <th>678</th>
          <td>7.0</td>
          <td>2</td>
          <td>3.62037</td>
          <td>2.713000e-02</td>
          <td>-0.153770</td>
        </tr>
        <tr>
          <th>683</th>
          <td>7.0</td>
          <td>2</td>
          <td>5.11465</td>
          <td>6.170000e-03</td>
          <td>0.217790</td>
        </tr>
        <tr>
          <th>797</th>
          <td>9.0</td>
          <td>2</td>
          <td>3.22398</td>
          <td>4.021000e-02</td>
          <td>0.093500</td>
        </tr>
        <tr>
          <th>821</th>
          <td>9.0</td>
          <td>2</td>
          <td>4.44742</td>
          <td>1.194000e-02</td>
          <td>-0.322870</td>
        </tr>
        <tr>
          <th>878</th>
          <td>10.0</td>
          <td>2</td>
          <td>5.82119</td>
          <td>3.070000e-03</td>
          <td>-0.017420</td>
        </tr>
        <tr>
          <th>883</th>
          <td>10.0</td>
          <td>2</td>
          <td>5.22966</td>
          <td>5.500000e-03</td>
          <td>0.267570</td>
        </tr>
      </tbody>
    </table>
    </div>

We would get a boolean series if we tried something else such as
``df.ix[:, 'p'] < 0.05``.

.. code-block:: py

   >>> df.ix[:, 'p'] < 0.05

.. parsed-literal::

    0      False
    1      False
    2      False
    3      False
    4      False
    5      False
    6      False
    7      False
    8      False
    9      False
    10     False
    11     False
    12     False
    13     False
    14     False
    15     False
    16      True
    17     False
    18     False
    19     False
    20     False
    21     False
    22     False
    23     False
    24     False
    25     False
    26     False
    27      True
    28     False
    29      True
           ...
    913    False
    914    False
    915    False
    916    False
    917    False
    918    False
    919    False
    920    False
    921    False
    922    False
    923    False
    924    False
    925    False
    926    False
    927    False
    928    False
    929    False
    930    False
    931    False
    932    False
    933    False
    934    False
    935    False
    936    False
    937    False
    938    False
    939    False
    940    False
    941    False
    942    False
    Name: p, dtype: bool


Any if condition can be used to select subset this way. We can also use
multiple conditions. Here is the syntax for using multiple conditions.

.. code-block:: py
   :caption: Selecting based upon multiple conditions.

   >>> df.ix[(df.ix[:, 'p'] < 0.05) & (df.ix[:, 'difference'] > 0.0)]

.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Chr</th>
          <th>df</th>
          <th>F</th>
          <th>p</th>
          <th>add_effect</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>27</th>
          <td>1.0</td>
          <td>2</td>
          <td>64.13061</td>
          <td>6.350400e-27</td>
          <td>1.92456</td>
        </tr>
        <tr>
          <th>243</th>
          <td>3.0</td>
          <td>2</td>
          <td>136.63303</td>
          <td>3.713800e-53</td>
          <td>-2.56580</td>
        </tr>
        <tr>
          <th>297</th>
          <td>3.0</td>
          <td>2</td>
          <td>7.61282</td>
          <td>5.234000e-04</td>
          <td>-0.47157</td>
        </tr>
        <tr>
          <th>364</th>
          <td>4.0</td>
          <td>2</td>
          <td>7.50622</td>
          <td>5.813500e-04</td>
          <td>-0.56912</td>
        </tr>
        <tr>
          <th>374</th>
          <td>4.0</td>
          <td>2</td>
          <td>37.85950</td>
          <td>1.423700e-16</td>
          <td>-1.62750</td>
        </tr>
        <tr>
          <th>663</th>
          <td>7.0</td>
          <td>2</td>
          <td>4.61881</td>
          <td>1.008000e-02</td>
          <td>-0.36436</td>
        </tr>
      </tbody>
    </table>
    </div>

Operating on the :class:`DataFrame`
===================================

:py:mod:`pandas` provides very advanced ways to work with data structures; however,
at the basic level we can treat them just like :py:mod:`numpy` arrays. For example

.. code-block:: py
   :caption: Using basic computational tools

   >>> df.mean(axis=0)
   Chr                  5.093319
   df                   1.995758
   F                    1.068350
   p                    0.578914
   add_effect           0.000041
   add_F                1.002238
   add_p                0.576336
   dom_effect           0.013870
   dom_F                1.018539
   dom_p                0.496334
   q                    0.977950
   minor_allele         1.673383
   minor_frequency      0.201507
   major_allele         1.720042
   major_frequency      0.798493
   locus              743.347826
   alpha_allele         1.728526
   alpha_frequency      0.788852
   alpha_effect         0.009953
   beta_allele          1.664899
   beta_frequency       0.211148
   beta_effect          0.006947
   difference           0.008203
   dtype: float64



For summary statistics we can use ``.describe``.

.. code-block:: py

   >>> df.describe()

.. raw:: html

    <div>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Chr</th>
          <th>df</th>
          <th>F</th>
          <th>p</th>
          <th>add_effect</th>
          <th>add_F</th>
          <th>add_p</th>
          <th>dom_effect</th>
          <th>dom_F</th>
          <th>dom_p</th>
          <th>...</th>
          <th>major_allele</th>
          <th>major_frequency</th>
          <th>locus</th>
          <th>alpha_allele</th>
          <th>alpha_frequency</th>
          <th>alpha_effect</th>
          <th>beta_allele</th>
          <th>beta_frequency</th>
          <th>beta_effect</th>
          <th>difference</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>count</th>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>9.430000e+02</td>
          <td>939.000000</td>
          <td>939.000000</td>
          <td>9.390000e+02</td>
          <td>939.000000</td>
          <td>9.390000e+02</td>
          <td>939.000000</td>
          <td>...</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
          <td>943.000000</td>
        </tr>
        <tr>
          <th>mean</th>
          <td>5.093319</td>
          <td>1.995758</td>
          <td>1.068350</td>
          <td>5.789143e-01</td>
          <td>0.000041</td>
          <td>1.002238</td>
          <td>5.763360e-01</td>
          <td>0.013870</td>
          <td>1.018539e+00</td>
          <td>0.496334</td>
          <td>...</td>
          <td>1.720042</td>
          <td>0.798493</td>
          <td>743.347826</td>
          <td>1.728526</td>
          <td>0.788852</td>
          <td>0.009953</td>
          <td>1.664899</td>
          <td>0.211148</td>
          <td>0.006947</td>
          <td>0.008203</td>
        </tr>
        <tr>
          <th>std</th>
          <td>2.874920</td>
          <td>0.065025</td>
          <td>5.147785</td>
          <td>2.814450e-01</td>
          <td>0.236670</td>
          <td>5.793395</td>
          <td>2.778293e-01</td>
          <td>0.203363</td>
          <td>1.481506e+00</td>
          <td>0.286540</td>
          <td>...</td>
          <td>1.189163</td>
          <td>0.120300</td>
          <td>423.291244</td>
          <td>1.183980</td>
          <td>0.141919</td>
          <td>0.138130</td>
          <td>1.410496</td>
          <td>0.141919</td>
          <td>0.093642</td>
          <td>0.111914</td>
        </tr>
        <tr>
          <th>min</th>
          <td>1.000000</td>
          <td>1.000000</td>
          <td>0.000386</td>
          <td>3.713800e-53</td>
          <td>-2.565800</td>
          <td>0.000005</td>
          <td>5.975200e-35</td>
          <td>-0.882110</td>
          <td>9.978500e-08</td>
          <td>0.000147</td>
          <td>...</td>
          <td>0.000000</td>
          <td>0.502000</td>
          <td>1.000000</td>
          <td>0.000000</td>
          <td>0.185500</td>
          <td>0.000000</td>
          <td>0.000000</td>
          <td>0.039500</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>25%</th>
          <td>3.000000</td>
          <td>2.000000</td>
          <td>0.191435</td>
          <td>3.642100e-01</td>
          <td>-0.086955</td>
          <td>0.052625</td>
          <td>3.587050e-01</td>
          <td>-0.090141</td>
          <td>1.105550e-01</td>
          <td>0.250695</td>
          <td>...</td>
          <td>1.000000</td>
          <td>0.730750</td>
          <td>387.500000</td>
          <td>1.000000</td>
          <td>0.730000</td>
          <td>0.000000</td>
          <td>0.000000</td>
          <td>0.118000</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>50%</th>
          <td>5.000000</td>
          <td>2.000000</td>
          <td>0.490400</td>
          <td>6.106300e-01</td>
          <td>-0.001462</td>
          <td>0.265640</td>
          <td>6.063900e-01</td>
          <td>0.013040</td>
          <td>4.574800e-01</td>
          <td>0.498960</td>
          <td>...</td>
          <td>2.000000</td>
          <td>0.861000</td>
          <td>738.000000</td>
          <td>2.000000</td>
          <td>0.861000</td>
          <td>0.000000</td>
          <td>2.000000</td>
          <td>0.139000</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>75%</th>
          <td>8.000000</td>
          <td>2.000000</td>
          <td>1.011045</td>
          <td>8.252050e-01</td>
          <td>0.090575</td>
          <td>0.843220</td>
          <td>8.186100e-01</td>
          <td>0.103350</td>
          <td>1.320980e+00</td>
          <td>0.739590</td>
          <td>...</td>
          <td>2.000000</td>
          <td>0.882000</td>
          <td>1105.500000</td>
          <td>2.000000</td>
          <td>0.882000</td>
          <td>0.000000</td>
          <td>3.000000</td>
          <td>0.270000</td>
          <td>0.000000</td>
          <td>0.000000</td>
        </tr>
        <tr>
          <th>max</th>
          <td>10.000000</td>
          <td>2.000000</td>
          <td>136.633030</td>
          <td>9.996100e-01</td>
          <td>1.924560</td>
          <td>164.448290</td>
          <td>9.982100e-01</td>
          <td>1.676050</td>
          <td>1.451941e+01</td>
          <td>0.999750</td>
          <td>...</td>
          <td>5.000000</td>
          <td>0.960500</td>
          <td>1476.000000</td>
          <td>5.000000</td>
          <td>0.960500</td>
          <td>2.773924</td>
          <td>5.000000</td>
          <td>0.814500</td>
          <td>1.892881</td>
          <td>2.477807</td>
        </tr>
      </tbody>
    </table>
    <p>8 rows × 23 columns</p>
    </div>



You can look at the whole list at http://pandas.pydata.org/pandas-docs/stable/computation.html#method-summary