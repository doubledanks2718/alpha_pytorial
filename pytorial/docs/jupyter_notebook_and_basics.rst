###############################
Working in the Jupyter Notebook
###############################

Go back to your terminal and run:

.. code-block:: console

   $ jupyter notebook
   [I 23:37:39.605 NotebookApp] Serving notebooks from local directory: <current directory>
   [I 23:37.39.605 NotebookApp] 0 active kernels
   [I 23:37.39.605 NotebookApp] The Jupyter Notebook is running at http://localhost:8888/
   [I 23:37.39.605 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).

This should open up a tab in your web browser titled ``Home`` and should look something like this:

.. image:: images/jupyter_notebook_home.PNG

.. note:: If the notebook screen does not open up for you put your red sticky up on your laptop.

Basic Information About Jupyter Notebook
========================================

The ``Jupyter Notebook`` provides an enhanced interface to the ``Python`` interpreter.
It is simple and intuitive to use. ``Jupyter Notebook`` files have the extension .ipynb.
If we were to open one of these files with a text editor we would see a large amount of
JSON. Our web browser renders the JSON and presents us with a pretty and functional
interface to the ``Python`` interpreter.

.. Note::

   ``Jupyter Notebooks`` are local files on your machine. We are merely using
   the web browser to view them. Your ``Jupyter Notebooks`` cannot be seen by others.

In the regular ``Python`` interpreter we type our command and then press ``Enter`` to
execute the command. In the Jupyter Notebook we use either ``Shift + Enter`` or
``Ctrl + Enter``. ``Shift + Enter`` will run any code in the cell and automatically
move to the next cell. ``Ctrl+Enter`` will only run the cell.

The Basics
==========

This section will introduce basic operations in ``Python`` and let you get a feel
for ``Python``'s syntax. The ``+`` and ``*`` operators are overloaded meaning
that they do more than numerical addition and multiplication.

Operators +, *
--------------

Type 3 + 4 into your notebook and press ``Shift + Enter``.

.. code-block:: py

    >>> 3 + 4
    7

You will notice that the expression ``3 + 4`` is evaluated and we have the next
cell selected.

    >>> 4 * 5
    20

Concatenation and Repetition
----------------------------

We can also use ``+`` and ``*`` to operate on strings and lists.

.. code-block:: py

    >>> 'alpha' + '_' + 'go'
    alpha_go

    >>> [3, 4, 5] + [6, 7, 8]
    [3, 4, 5, 6, 7, 8]

    >>> 'alpha'*3
    alphaalphaalpha

    >>> [3, 4, 5]*3
    [3, 4, 5, 3, 4, 5, 3, 4, 5]


Variable Assignment
-------------------

We can set variables in ``Python`` by using the assignment operator
otherwise known as an equals sign: ``=``.

   >>> my_first_variable = 'pi'
   >>> my_second_variable = 3.14

Iteration
---------

You will find yourself needing to iterate or loop over something at some point.
I use ``for`` loops ubiquitously in my work.

   >>> elements = [3, 4, 5, 6, 'eight', 'nine', 'ten']
   >>> for item in elements:
   ... print(item)
   3
   4
   5
   6
   eight
   nine
   ten

Many other languages require you to iterate over the indexes of the object.
In ``Python`` and ``R`` we can iterate directly over the contents of the container.

.. _dynamic_variable_types:

Dynamic Variable Types
======================

``Python`` variables are **dynamically typed** meaning they can be changed
from one type to another. For example nearly everything can be changed
into a string using the :py:func:`str` ; however, not everything can be made
into an integer using :py:func:`int`

.. code-block:: py

   >>> number = 4.0
   >>> str(number)
   4.0
   >>> list_of_numbers = [3, 4, 5]
   >>> str(list_of_numbers)
   '[3, 4, 5]'

.. _type_function:

Occasionally you may run into errors because you do not have the correct data
type for the process you are trying to run. However, the creators of ``Python``
foresaw this problem. We can examine the **type** of an object using the :py:func:`type` function.

.. code-block:: py

      >>> type(number)
      float
      >>> type(str(number))
      str
      >>> type(str) == type(number)
      False
      >>> type(list_of_numbers)
      list
      >>> type(str(list_of_numbers))
      str

.. _end_jupyter_notebook_and_basics:

Next Lesson
===========

I feel that tutorials for a specific task are more useful than showing a
bunch of unrelated features of ``Python`` with no real goal in mind. I would
greatly appreciate your feedback at the end of these lessons. Now that we have
the fundamentals of ``Python`` down we will move onto learning how to parse a **GFF3** file.