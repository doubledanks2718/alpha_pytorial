.. _path_variable:

#################
The PATH Variable
#################

In rough terms the ``PATH`` variable is a list of programs your computer is able to *see*.
We will use the installation and setup of the ``nano`` text editor as an exercise
in manipulating the ``PATH`` variable. The `GNU nano`_ editor is a cross-platform
text editor which can be used within the terminal. It is very useful for on-the-fly
edits of text and source code. In particular you might find it useful for use
for working on a cluster such as Biohen_

.. _Biohen: http://bioinformatics.udel.edu/Core/HPC
.. _GNU nano: http://www.nano-editor.org/

Getting Started with ``nano``
=============================

.. _instructions_for_windows:

Windows
~~~~~~~

If you ordinarily use ``Windows`` (such as myself) then you are used to installing
things by double clicking on a file. The :program:`nano` does not have a pre-made
binary installer which we are used to. Instead we have to download the source
code and then manually tell our computer where the executable file is. Download
the the :program:`nano` source code here_.

.. _here: https://www.nano-editor.org/dist/v2.5/NT/

For ``Windows`` download the file :file:`nano-2.5.3.zip`. For the sake of this exercise
make a folder in the :file:`C` directory called :file:`path_exercise`. Extract the contents
of :file:`nano-2.5.3.zip` into the folder :file:`C:\\path_exercise`.

.. image:: images/path_exercise.PNG

Editing the :envvar:`PATH` Variable
===================================

In order for our computer to be able to use :program:`nano` our computer needs to
be able to *see* the :file:`nano.exe` file. The :file:`nano.exe` is in
:file:`C:\\path_exercise\\nano-2.5.3-win32`. To edit your :envvar:`PATH` variable
press the windows key on your keyboard. Type ``sys``. This will
bring up a menu. Choose ``Edit the system environment variables`` Selecting the option
``Environment variables`` which will bring up the following menu:

.. image:: images/env_variable_menu.PNG

Under the user environment variables select the :envvar:`PATH` variable and press
``Edit``. Which will give you a menu like this:

.. image:: images/path_variable_value.PNG

Now go to the end of the list in ``Variable value:`` and add this text:

``;C:\path_exercise\nano-2.5.3-win32`` and then press ``OK``.

Now open a terminal by pressing the windows key. Type ``cmd.exe`` and press ``Enter``.
Type :command:`nano` into the terminal and press ``Enter``. You should see something
similar to the following window:

.. image:: images/nano_in_terminal.PNG

.. _instructions_for_unix:

Linux and Mac
=============

In Linux and Mac which use the :program:`bash` shell it is simple to manipulate
the :envvar:`PATH` variable using the command line. Editing the :envvar:`PATH`
should be done in two steps:

   1) Temporarily edit :envvar:`PATH` and test the results
   2) Assuming you have confirmed that you have changed :envvar:`PATH` correctly edit the :file:`.bashrc` or :file:`.bash_profile` files

So for example in a terminal we would use the :command:`export` command to
temporarily change :envvar:`PATH`:

.. code-block:: console


   $ export PATH=$PATH:/somedirectory/:/some/other/directory/
   $ echo "$PATH"
   /usr/local/whatever/:/somedirectory/:/some/other/directory/

It is important to test that you have done this correctly because you can easily
wipe out your :envvar:`PATH` variable. If your computer cannot see the :program:`bash`
executable you will be unable to use common commands such as :command:`ls` or
:command:`pwd`. I have deleted my :envvar:`PATH` before but luckily I remembered
the location of the :program:`bash` executable.

